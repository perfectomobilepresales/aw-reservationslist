import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.io.StringReader;

public class getModelforDeviceId {

    public static String getModelforDevice(String cloud, String cloudUser, String cloudPassword, String deviceID) throws IOException, ParserConfigurationException, SAXException {

        String rtnModel = null;
        HttpClient client = HttpClientBuilder.create().build();
        HttpGet request = new HttpGet("https://" + cloud + "/services/handsets/" + deviceID + "?operation=info&user=" + cloudUser + "&password=" + cloudPassword);

        HttpResponse response = client.execute(request);
        HttpEntity resEntity = response.getEntity();
        String responseStr = EntityUtils.toString(resEntity);
        //System.out.println("device info: " + responseStr);

        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder;
        InputSource is;
        String deviceModels = null;
        try {
            builder = factory.newDocumentBuilder();
            is = new InputSource(new StringReader(responseStr));
            Document doc = builder.parse(is);
            NodeList list = doc.getElementsByTagName("model");

            for (int i = 0; i < list.getLength(); i++) {
                Element ele = (Element) list.item(i);
                String deviceModel = list.item(i).getTextContent();
                if (i == 0) {
                    deviceModels = deviceModel;
                } else {
                    deviceModels = deviceModels + "," + deviceModel;
                }
                //System.out.println(deviceModels);
            }
        } catch (ParserConfigurationException e) {
        } catch (SAXException e) {
        } catch (IOException e) {
        }

        rtnModel = deviceModels;
        return rtnModel;
    }
}

