import org.apache.commons.lang3.text.WordUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringReader;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.atomic.AtomicLong;

/**
 * This template is for users that use DigitalZoom Reporting (ReportiumClient).
 * For any other use cases please see the basic template at https://github.com/PerfectoCode/Templates.
 * For more programming samples and updated templates refer to the Perfecto Documentation at: http://developers.perfectomobile.com/
 */
public class getReservationsList {

    public static void main(String[] args) throws IOException, ParserConfigurationException, SAXException {
        System.out.println("Run started");

        try {
            // Set cloud host and credentials values from CI, else use local values
            String PERFECTO_HOST = System.getProperty("np.testHost", System.getenv().get("PERFECTO_CLOUD"));
            String PERFECTO_USER = System.getProperty("np.testUsername", System.getenv().get("PERFECTO_CLOUD_USERNAME"));
            String PERFECTO_PASSWORD = System.getProperty("np.testPassword", System.getenv().get("PERFECTO_CLOUD_PASSWORD"));

            StringBuilder resourceIDs = null;

            HttpClient client = HttpClientBuilder.create().build();
            HttpGet request = new HttpGet("https://" + PERFECTO_HOST + "/services/handsets?operation=list&user=" + PERFECTO_USER + "&password=" + PERFECTO_PASSWORD);

            HttpResponse response = client.execute(request);
            HttpEntity resEntity = response.getEntity();
            String responseStr = EntityUtils.toString(resEntity);

            //now parse the response to build a list of connected devices

            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder;
            InputSource is;
            try {
                builder = factory.newDocumentBuilder();
                is = new InputSource(new StringReader(responseStr));
                Document doc = builder.parse(is);
                NodeList list = doc.getElementsByTagName("deviceId");

                for (int i = 0; i < list.getLength(); i++) {
                    Element ele = (Element) list.item(i);
                    String deviceID = ele.getTextContent();
                    if (i == 0) {
                        resourceIDs = new StringBuilder(deviceID);
                    } else {
                        resourceIDs.append(",").append(deviceID);
                    }
                    //System.out.println(resourceIDs);
                }
            } catch (ParserConfigurationException | SAXException | IOException e) {
                e.printStackTrace();
            }


            //OK now get the reservations
            Date date = new Date();
            long startTime;
            startTime = date.getTime();
            long endTime;
            int windowHours = 8;
            int windowMilliSecs = windowHours * 3600 * 1000;
            endTime = startTime + windowMilliSecs;

            request = new HttpGet("https://" + PERFECTO_HOST + "/services/reservations?operation=list&user=" + PERFECTO_USER + "&password=" + PERFECTO_PASSWORD + "&admin=true&resourceIDs=" + resourceIDs + "&startTime=" + startTime + "&endTime=" + endTime);

            response = client.execute(request);
            resEntity = response.getEntity();
            responseStr = EntityUtils.toString(resEntity);
            //System.out.print(responseStr);

            //Finally parse the returned JSON to make it easier to read
            final JSONObject obj = new JSONObject(responseStr);
            final JSONArray resdata = obj.getJSONArray("reservations");
            final int n = resdata.length();
            String resId;
            String resourceId;
            String reservedTo;
            Object startTimejson;
            String startTimeStr;
            Object endTimejson;
            String endTimeStr;
            String reservedBy;
            String status;
            Object createdDatejson;
            String createdDateStr;
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MMM-dd HH:mm:ss z");

            try {
                PrintWriter writer = new PrintWriter("reports/ReservationsReport.html", "UTF-8");
                writer.println("<!DOCTYPE html>");
                writer.println("<html>");
                writer.println("<head>");
                writer.println("<title>Reservations List</title>");
                writer.println("<h1>" + WordUtils.capitalize(PERFECTO_HOST) + " Cloud Reservations List</h1>");
                writer.println("<h3>last run date: " + sdf.format(startTime) + "</h3>");
                writer.println("</head>");
                writer.println("<body>");
                writer.println("<table border=\"1\">");
                writer.println("<tr>");
                writer.println("<th>Reservation ID</th>");
                writer.println("<th>Device ID</th>");
                writer.println("<th>Device Model</th>");
                writer.println("<th>Reserved to User</th>");
                writer.println("<th>Reservation Start</th>");
                writer.println("<th>Reservation End</th>");
                writer.println("<th>Reserved By</th>");
                writer.println("<th>Status</th>");
                writer.println("<th>Created</th>");
                writer.println("</tr>");

                for (int i = 0; i < n; ++i) {

                    writer.println("<tr>");

                    final JSONObject reservation = resdata.getJSONObject(i);
                    resId = reservation.getString("id");
                    writer.println("<td>" + resId + "</td>");

                    resourceId = reservation.getString("resourceId");
                    writer.println("<td>" + resourceId + "</td>");

                    String deviceModel = getModelforDeviceId.getModelforDevice(PERFECTO_HOST, PERFECTO_USER, PERFECTO_PASSWORD, resourceId);
                    writer.println("<td>" + deviceModel + "</td>");

                    reservedTo = reservation.getString("reservedTo");
                    writer.println("<td>" + reservedTo + "</td>");

                    startTimejson = (reservation.get("startTime"));
                    startTimeStr = startTimejson.toString();
                    String[] millisStr = startTimeStr.split("millis\":\"");
                    String subStringMillis = null;
                    for (String t : millisStr)
                        subStringMillis = t.substring(0, 13);
                    AtomicLong startTimeMillis = new AtomicLong(Long.parseLong(subStringMillis));
                    Date startDatetime = new Date(startTimeMillis.get());
                    writer.println("<td>" + sdf.format(startDatetime) + "</td>");

                    endTimejson = reservation.get("endTime");
                    endTimeStr = endTimejson.toString();
                    String[] millisStr1 = endTimeStr.split("millis\":\"");
                    String subStringMillis1 = null;
                    for (String t : millisStr1)
                        subStringMillis1 = t.substring(0, 13);
                    long endTimeMillis = Long.parseLong(subStringMillis1);
                    Date endDatetime = new Date(endTimeMillis);
                    writer.println("<td>" + sdf.format(endDatetime) + "</td>");

                    reservedBy = reservation.getString("reservedBy");
                    writer.println("<td>" + reservedBy + "</td>");

                    status = reservation.getString("status");
                    writer.println("<td>" + status + "</td>");

                    createdDatejson = reservation.get("created");
                    createdDateStr = createdDatejson.toString();
                    String[] millisStr2 = createdDateStr.split("millis\":\"");
                    String subStringMillis2 = null;
                    for (String t : millisStr2)
                        subStringMillis2 = t.substring(0, 13);
                    long createdTimeMillis = Long.parseLong(subStringMillis2);
                    Date createdDatetime = new Date(createdTimeMillis);
                    writer.println("<td>" + sdf.format(createdDatetime) + "</td>");

                    writer.println("</tr>");
                }

                writer.println("</table>");
                writer.println("</body>");
                writer.println("</html>");
                writer.close();

            } catch (IOException e) {
                e.printStackTrace();
            }


        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println("Run ended");
    }
}
